FROM node:21-alpine3.18

RUN mkdir /home/app
COPY app /home/app
WORKDIR /home/app
EXPOSE 3000
RUN npm install
CMD [ "node", "server.js" ]